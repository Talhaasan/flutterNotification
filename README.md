# Flutter Notification App

Uygulama içinden ve Firebase kullanarak bildirim gönderilebilen Flutter uygulaması.

1) Projeyi lokal ortamınıza çekin.

git clone https://gitlab.com/Talhaasan/flutterNotification.git

2) Proje dizininize gidin ve aşağıdaki komutları sırasıyla çalıştırın.

flutter pub get

flutter run

3) Uygulama içinden bildirim göndermeye başlayabilirsiniz. Bildirimleri Firebase üzerinden göndermek istiyorsanız da uygulama dosyalarında benim hesabıma ait olan google-services-json dosyası yerine kendi Firebase hesabınıza ait dosyayı koyabilirsiniz.   
